from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import ScrumyGoals, GoalStatus
from django.contrib.auth.models import User, Group
from .forms import SignupForm, CreateGoalForm
import random

# Create your views here.


def index(request):
    # goal = ScrumyGoals.objects.filter(goal_name='Learn Django')
    # return HttpResponse(goal)

    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        form.save()
        user = User.objects.get(username=request.POST['username'])
        my_group = Group.objects.get(name='Developer')
        my_group.user_set.add(user)

        if user.groups.filter(name='Developer').exists():
            return render(request, 'jeremiahscrumy/welcome.html')
    else:
        form = SignupForm()

    context = {'form': form}

    return render(request, 'jeremiahscrumy/index.html', context)


def add_goal(request):

    # rand_num = random.randrange(1000, 9999, 1)
    # my_user = User.objects.get(username='louis')
    # weekly_goal = GoalStatus.objects.get(status_name='Weekly Goal')

    # goal_added = ScrumyGoals.objects.create(goal_name='Keep Learning Django', goal_id=rand_num, created_by='Louis',
    #                                         moved_by='Louis', owner='Louis', goal_status=weekly_goal, user=my_user)
    # return HttpResponse(goal_added)

    form = CreateGoalForm()
    if request.method == 'POST':
        form = CreateGoalForm(request.POST)
        good_to_go = form.save(commit=False)
        good_to_go.goal_id = random.randrange(1000, 9999, 1)
        good_to_go.created_by = 'Louis'
        good_to_go.moved_by = 'Louis'
        good_to_go.owner = 'Louis'
        good_to_go.goal_status = GoalStatus(status_name='Weekly Goal')
        good_to_go.goal_status.save()
        good_to_go.save()

    else:
        form = CreateGoalForm()

    context = {'form': form}

    return render(request, 'jeremiahscrumy/goal.html', context)


def move_goal(request, **kwargs):
    # try:
    #     goal = ScrumyGoals.objects.get(goal_id=goal_id)
    # except ScrumyGoals.DoesNotExist:
    #     raise Http404("A record with that goal id does not exist")

    #     context = {'error': goal}
    #     return render(request, 'jeremiahscrumy/exception.html', context)
    # else:
    #     return HttpResponse(goal.goal_name)

    dictionary = {'result': 'Success'}
    if request.method == 'POST':
        if request.user.groups.filter(name="Developer").exists():
            obj = ScrumyGoals.objects.get(goal_id=kwargs['goal_id'])
            if request.user == obj.user:
                if request.POST['goal_status'] == 'Done Goal':
                    return HttpResponse('You are a developer')
                else:
                    obj.goal_status = GoalStatus(
                        status_name=request.POST['goal_status'])
                    obj.goal_status.save()
                    obj.save()
                    return render(request, 'jeremiahscrumy/exception.html', dictionary)
            else:
                return HttpResponse('Not the owner')
        elif request.user.groups.filter(name='Quality Assurance').exists():
            obj = ScrumyGoals.objects.get(goal_id=kwargs["goal_id"])
            if request.user == obj.user:
                obj.goal_status = GoalStatus(
                    status_name=request.POST['goal_status'])
                obj.goal_status.save()
                obj.save()
                return render(request, 'jeremiahscrumy/exception.html', dictionary)
            else:
                if request.POST['goal_status'] == "Done Goal":
                    obj.goal_status = GoalStatus(
                        status_name=request.POST['goal_status'])
                    obj.goal_status.save()
                    obj.save()
                    return render(request, 'jeremiahscrumy/exception.html', dictionary)
                else:
                    return HttpResponse('Not the owner')
        elif request.user.groups.filter(name='Owner').exists():
            obj = ScrumyGoals.objects.get(goal_id=kwargs["goal_id"])
            if request.user == obj.user:
                obj.goal_status = GoalStatus(
                    status_name=request.POST['goal_status'])
                obj.goal_status.save()
                obj.save()
                return render(request, 'jeremiahscrumy/exception.html', dictionary)
            else:
                return HttpResponse('Not the owner')
        elif request.user.groups.filter(name='Admin').exists():
            obj = ScrumyGoals.objects.get(goal_id=kwargs["goal_id"])
            obj.goal_status = GoalStatus(
                status_name=request.POST['goal_status'])
            obj.goal_status.save()
            obj.save()
            return render(request, 'jeremiahscrumy/exception.html', dictionary)
        else:
            return render(request, 'jeremiahscrumy/exception.html')
    else:
        return render(request, 'jeremiahscrumy/exception.html')


def home(request):
    dictionary = {}
    users = User.objects.all()
    weekly_goals = ScrumyGoals.objects.filter(
        goal_status=GoalStatus.objects.get(status_name='Weekly Goal'))
    daily_goals = ScrumyGoals.objects.filter(
        goal_status=GoalStatus.objects.get(status_name='Daily Goal'))
    verify_goals = ScrumyGoals.objects.filter(
        goal_status=GoalStatus.objects.get(status_name='Verify Goal'))
    done_goals = ScrumyGoals.objects.filter(
        goal_status=GoalStatus.objects.get(status_name='Done Goal'))

    dictionary['weekly_goals'] = '  '.join(
        [eachgoal_1.goal_name for eachgoal_1 in weekly_goals])
    dictionary['daily_goals'] = '  '.join(
        [eachgoal_1.goal_name for eachgoal_1 in daily_goals])
    dictionary['verify_goals'] = '  '.join(
        [eachgoal_1.goal_name for eachgoal_1 in verify_goals])
    dictionary['done_goals'] = '  '.join(
        [eachgoal_1.goal_name for eachgoal_1 in done_goals])

    dictionary['users'] = users

    dictionary['user_real'] = '  '.join(
        [eachgoal_1.username for eachgoal_1 in users])
    return render(request, 'jeremiahscrumy/home.html', dictionary)

    # my_users = User.objects.all()
    # my_first_user = my_users[0]
    # my_goals = my_first_user.user.all()
    # goal_status = GoalStatus.objects.all()
    # weekly_goal = goal_status[0]
    # daily_goal = goal_status[1]
    # verify_goal = goal_status[2]
    # done_goal = goal_status[3]

    # dictionary = {
    #     'my_first_user': my_first_user,
    #     'weekly_goal': weekly_goal,
    #     'daily_goal': daily_goal,
    #     'verify_goal': verify_goal,
    #     'done_goal': done_goal,
    #     'my_goals': my_goals,
    #     'my_users': my_users,
    # }

    return render(request, 'jeremiahscrumy/home.html', dictionary)
