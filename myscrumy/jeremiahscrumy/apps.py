from django.apps import AppConfig


class JeremiahscrumyConfig(AppConfig):
    name = 'jeremiahscrumy'
