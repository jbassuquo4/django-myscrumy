import os
from setuptools import setup, find_packages

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now
# 1) we have a top level README file and
# 2) it's easier to type in the README file than to put a raw string in below ...


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="django-jeremiahscrumy",
    version="1.0.0",
    author="Jeremiah Asuquo",
    author_email="jbassuquo4@gmail.com",
    description=("A demonstration of how to create, document, and publish "
                 "the jeremiahscrumy app."),
    license="BSD",
    keywords="lab21 django internship",
    # url="http://packages.python.org/an_example_pypi_project",
    packages=find_packages(),
    long_description=read('README.rst'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)

# setup(
#     name='jeremiahscrumy',
#     version='1.0',
#     packages=find_packages(),
#     scripts=['manage.py']
# )
