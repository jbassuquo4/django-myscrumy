# Generated by Django 3.1.5 on 2021-02-03 17:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jeremiahscrumy', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrumygoals',
            name='goal_status',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='jeremiahscrumy.goalstatus'),
        ),
    ]
