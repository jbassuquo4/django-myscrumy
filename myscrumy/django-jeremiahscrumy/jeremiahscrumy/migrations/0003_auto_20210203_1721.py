# Generated by Django 3.1.5 on 2021-02-03 17:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('jeremiahscrumy', '0002_auto_20210203_1718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrumygoals',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='user', to=settings.AUTH_USER_MODEL),
        ),
    ]
